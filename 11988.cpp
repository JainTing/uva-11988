#include <iostream>
#include <string>

using namespace std;
struct List{
  char C;
  List * Next;
};
int main(){
  string character; 
  while(getline(cin, character) , !cin.eof()){
    List * head = NULL;
    List * temp = NULL;
    List * now = NULL;
    for(string::size_type i = 0 ; i < character.size() ; i++){
      temp = new List;
      temp->C = character[i];
      temp->Next = NULL;
      if(head == NULL){
	head = temp;
	now = temp;
      }
      else{
	if(character[i] =='['){
	  temp->Next = head;
	  head = temp;
	  now = temp;
	}
	else if(character[i] == ']'){
	  List * walker = now;
	  while(walker->Next != NULL)
	    walker = walker->Next;
	  walker->Next = temp;
	  now = temp;
	}
	else{
	  List * postnow = now->Next;
	  temp->Next = postnow;
	  now->Next = temp;
	  now = temp;
	}
      }    
    }     
    for(List * walker = head ; walker != NULL ; walker = walker->Next){
      if(walker->C != '[' && walker->C != ']')
	cout << walker->C;
    }
    cout << endl;
    delete temp;
  }   
  return 0;
}
